 <?php
/**
* Add theme support option
* @package custom_theme
*/

/*
======================
Activate Nav Menu Option
======================
 */
function custom_theme_register_nav_menu(){
    add_theme_support( 'menus' );
    register_nav_menu('primary', 'Header Navigation Menu');
    register_nav_menu('secondary', 'Footer Navigation Menu');
}
add_action( 'init', 'custom_theme_register_nav_menu');

/*
======================
Custom logo admin setup
======================
 */
function custom_theme_logo_setup() {
  $defaults = array(
      'height'      => 100,
      'width'       => 400,
      'flex-height' => false,
      'flex-width'  => false,
  );
  add_theme_support( 'custom-logo', $defaults );
}
add_action( 'init', 'custom_theme_logo_setup');

/*
======================
Custom widget setup
======================
 */
function custom_theme_widget_init() {
  register_sidebars(
    array(
      'name' => esc_html( 'custom_theme widget', 'custom_themetheme' ),
      'id' => 'custom_theme-widget',
      'description' => 'dynamic header widget',
      'before_widget' => '<section id="%1$s" class="custom_theme-widget" %2$s>',
      'after_widget' => '</section>',
      'before_title' => '<h2 class="custom_theme-widget-title"/>',
      'after_title' => '</h2>'
    )
  );
}
add_action( 'widgets_init', 'custom_theme_widget_init' );


add_theme_support( 'custom-header' );
add_theme_support( 'post-thumbnails' );

?>
