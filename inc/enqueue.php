<?php
/*
@package custom_theme
===========================
front-end enqueue functions
===========================
*/
function custom_theme_load_scripts() {
    //bootstrap 4.0.0 files
    wp_enqueue_style( 'bootstrap', get_template_directory_uri(). '/css/bootstrap.min.css', array(), '4.0.0', 'all' );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri(). '/js/bootstrap.min.js', array(), '4.0.0', true );

    //Add custom_theme css file in CSS folder generate by sass.
    wp_enqueue_style( 'custom_theme', get_template_directory_uri(). '/css/custom_theme.css', array(), '0.0.1', 'all' );
    wp_enqueue_script( 'custom_theme', get_template_directory_uri(). '/js/custom_theme.js', array('jquery'), '0.0.1', true );

    //font-awesome css
    wp_enqueue_style( 'font-awesome', get_template_directory_uri(). '/css/font-awesome.min.css', array(), '4.0.0', 'all' );

    //video plugin plyr css and js
    wp_enqueue_style( 'plyr', get_template_directory_uri(). '/css/plyr.css', array(), '1.0.0', 'all' );
    wp_enqueue_script( 'plyr', get_template_directory_uri(). '/js/plyr.js', array('jquery'), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'custom_theme_load_scripts');


function custom_theme_load_admin_scripts() {
    wp_enqueue_media();
    wp_enqueue_script( 'toolset', get_template_directory_uri(). '/inc/admin-script/admin.js', array('jquery'), '1.0.4', true );
}
add_action( 'admin_enqueue_scripts', 'custom_theme_load_admin_scripts');

?>
