<?php
/*
This is the template for the header
@package custom_themetheme
*/
?>
<!DOCTYPE html>
<html <?php language_attributes();  ?> >

<head>
    <title><?php wp_title(); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="<?php bloginfo('charset'); ?>">
    <link rel="profile" href="http:// gmpg.org/xfn/11">
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <?php if (is_singular() && pings_open(get_queried_object())): ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="container-fluid" style="padding-left: 0; padding-right: 0;">
		<!-- Begin Page header -->
				<?php
				if (is_front_page())  :
					get_template_part( 'template-parts/headers/header', 'home' );
				else:
					get_template_part( 'template-parts/headers/header', 'page' );
				endif;
				?>
		<!-- End Page header -->
