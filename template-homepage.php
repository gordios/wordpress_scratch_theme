<?php
/*
@package custom_theme

Template Name: Homepage
*/

//CUSTOM THEME POST START
$arg = array(
  'post_type' => 'custom_theme',
  'posts_per_page' => 1
);

$loop = new WP_Query( $arg );
if( $loop->have_posts()):
  while( $loop->have_posts() ): $loop->the_post();

    get_header();

    $template_parts = ['custom_theme','video'];

    foreach ($template_parts as $template_part) {
      get_template_part( 'template-parts/contents/content', $template_part );
    }

    get_footer();

  endwhile;
endif;
//CUSTOM THEME POST END

?>
