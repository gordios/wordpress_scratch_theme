<header class="text-center">
  <!-- header-top -->
  <?php get_template_part('template-parts/headers/header-parts/header', 'top') ?>
  <!-- main header -->
  <?php get_template_part('template-parts/headers/header-parts/header', 'main') ?>

</header> <!-- header -->
