<!-- Wordpress dynamic menu -->
<div class="collapse navbar-collapse text-center" id="navbarContent">
  <?php wp_nav_menu(array(
            'theme_location' => 'primary',
            'container' => false,
            'menu_class' => 'nav navbar-nav ml-auto',
            'walker' => new Walker_Nav_Primary()
        ));
  ?>
</div>
