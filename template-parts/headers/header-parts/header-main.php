<section class="header-main" id="header-main">

    <img class="header-main-logo pull-right"
         src="<?= get_template_directory_uri(). '/pictures/svg/header-main-logo.svg' ?>"
         alt="custom_theme">

  <?= get_custom_theme_meta_field('wpcf-banner-text') ?>


</section><!-- section -->
