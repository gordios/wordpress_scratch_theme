<?php
/**
 * This is the template for the footer
 * @package custom_theme
 */
?>
</div> <!--container-fluid-->

<?php
    get_template_part( 'template-parts/footers/footer', 'page' );
?>

<?php wp_footer(); ?>
</body>
</html>
